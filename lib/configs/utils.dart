import 'dart:convert';
import 'dart:io';

Future<String> loadTextFile(String filename) async {
  var file = File(filename);
  if (await file.exists()) {
    return await file.readAsString();
  }
  throw Exception('File "$filename" not found!');
}

Future<File> saveTextFile(String filename, String text) async {
  return await File(filename).writeAsString(text);
}

Future<Map<String, dynamic>> loadJsonFile(String filename) async {
  var file = File(filename);
  if (await file.exists()) {
    var source = await file.readAsString();
    var json = jsonDecode(source);
    return json;
  }
  throw Exception('File "$filename" not found!');
}
