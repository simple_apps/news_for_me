import 'dart:io';
// import 'package:news_for_me/configs/utils.dart';

// асинхронная singleton-заглушка для будущих более сложных вариантов
// инициализации приложения
class AppConfig {
  static final Map<String, dynamic> _config = {
    'use_local_storage': _isNotWebApp(), // Нужно для выбора хранилища настроек
    'http': {
      'port': 80,
      'api_key': 'e260572638104ababa38ea004120b1ed',
    },
  };

  bool _ready = false;

  Future<bool> initialize([String? filename]) async {
    if (!_ready) {
      var apiKey = _config['http']['api_key'];
      var port = _config['http']['port'];
      _config['http']['host'] = _config['http']['host'] = 'newsapi.org';
      _config['http']['route'] = 'v2/everything';
      _config['http']['params'] = <String, dynamic>{
        'domains': 'wsj.com',
        'apiKey': apiKey
      };
      _config['http']['url']='http://newsapi.org/v2/everything?domains=wsj.com&apiKey=$apiKey';
      _ready = true;
    }
    return _ready;
  }

  bool get useLocalStorage => _config['use_local_storage'] ?? false;
  Uri get apiUrl {
    if (!_ready) {
      throw Exception('AppConfig not initialized!');
    }
    // var port = _config['http']['port'] as int?;
    var url = Uri.parse(_config['http']['url']);
    return url;
    // return Uri(
    //     host: '${_config['http']['host']}',
    //     port: port == 80 ? null : port,
    //     path: '${_config['http']['route']}',
    //     queryParameters: _config['http']['params']);
  }

  static final AppConfig _instance = AppConfig._internal();
  factory AppConfig() {
    return _instance;
  }
  AppConfig._internal();

  static bool _isNotWebApp() {
    var res = true;
    try {
      Platform.operatingSystem;
    } catch (e) {
      // print('error: $e');
      res = false;
    }
    return res;
  }
}
