import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:news_for_me/models/newsInfo.dart';
import '../appstate.dart';
import 'news_card.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late final NewsBlocState bloc;
  late final CounterBlocState blocCounter;

  @override
  void initState() {
    super.initState();
    bloc = NewsBlocState();
    blocCounter =CounterBlocState();
    bloc.action.add(EventsBloc.fetchNews);
  }

  @override
  void dispose() {
    bloc.dispose();
    blocCounter.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: StreamBuilder<int>(
            stream: blocCounter.state,
            builder: (_, snapshot) {
              if (snapshot.hasData) {
                return Text('${widget.title} ${snapshot.data?.toString() ?? '0'}');
              } else {
                return Text(widget.title);
              }
            }),
      ),
      body: StreamBuilder<NewsInfo>(
          stream: bloc.state,
          builder: (_, snapshot) {
            if (snapshot.hasData) {
              return FutureBuilder<NewsInfo>(
                future: bloc.newsInfo,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var data = snapshot.data;
                    if (data == null) {
                      return const Center(
                        child: Text(
                          'error: Wrong FutureBuilder snapshot',
                        ),
                      );
                    } else if (data.status == 'error') {
                      return Center(
                        child: Text(
                          'error: ${data.errorMessage}',
                        ),
                      );
                    } else {
                      return ListView.builder(
                          itemCount: data.articles.length,
                          itemBuilder: (context, index) {
                            var article = data.articles[index];
                            return NewsCard(
                              title: article.title,
                              description: article.description,
                              urlToImage: article.urlToImage,
                              formattedTime: DateFormat('dd MMM - HH:mm')
                                  .format(article.publishedAt),
                            );
                          });
                    }
                  } else if (snapshot.hasError) {
                    return Center(
                      child: Text(
                        'error: ${snapshot.error}',
                      ),
                    );
                  } else {
                    return const Center(child: CircularProgressIndicator());
                  }
                },
              );
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          blocCounter.action.add(EventsBloc.increase);
          bloc.action.add(EventsBloc.fetchNews);
        },
        tooltip: 'fetch news',
        child: const Icon(Icons.update_outlined),
      ),
    );
  }
}

class _HomePageState2 extends State<HomePage> {
  late final CounterBlocState blocCounter;

  @override
  void initState() {
    super.initState();
    blocCounter = CounterBlocState();
  }

  @override
  void dispose() {
    blocCounter.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            StreamBuilder<int>(
                stream: blocCounter.state,
                builder: (_, snapshot) {
                  if (snapshot.hasData) {
                    return Text(
                      snapshot.data?.toString() ?? '0',
                      style: Theme.of(context).textTheme.headline4,
                    );
                  } else {
                    return Text(
                      '0',
                      style: Theme.of(context).textTheme.headline4,
                    );
                  }
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => blocCounter.action.add(EventsBloc.increase),
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
