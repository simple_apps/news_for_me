import 'package:flutter/material.dart';

class NewsCard extends StatelessWidget {
  GlobalKey? key;
  String title;
  String description;
  String urlToImage;
  String formattedTime;

  NewsCard(
      {this.key,
      String? title,
        String? description,
        String? urlToImage,
        String? formattedTime})
      : title = (title == null) ? '...' : title,
        description = (description == null) ? '...' : description,
        urlToImage = (urlToImage == null) ? '' : urlToImage,
        formattedTime = (formattedTime == null) ? '' : formattedTime,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      margin: const EdgeInsets.all(8),
      child: Row(
        children: <Widget>[
          Card(
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(24),
            ),
            child: AspectRatio(
                aspectRatio: 1,
                child: Image.network(
                  urlToImage,
                  fit: BoxFit.cover,
                )),
          ),
          const SizedBox(width: 16),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(formattedTime),
                Text(
                  title,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  description,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
