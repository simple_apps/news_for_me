import 'dart:async';

import 'middleware/newsapi.dart';
import 'models/newsInfo.dart';

enum EventsBloc { increase, fetchNews, getArticle}

class NewsBlocState {
  Future<NewsInfo> newsInfo = NewsApi().getNews();

  final _stateController = StreamController<NewsInfo>();
  final _eventController = StreamController<EventsBloc>();

  Stream<NewsInfo> get state => _stateController.stream;
  Sink<EventsBloc> get action => _eventController.sink;

  NewsBlocState() {
    _eventController.stream.listen(_handleEvent);
  }

  void dispose() {
    _stateController.close();
    _eventController.close();
  }

  void _handleEvent(EventsBloc action) {
    if (action == EventsBloc.fetchNews) {
      newsInfo = NewsApi().getNews();
    }
    newsInfo.then((value) => _stateController.add(value));
  }
}

class CounterBlocState {
  int value = 0;

  final _stateController = StreamController<int>();
  final _eventController = StreamController<EventsBloc>();

  Stream<int> get state => _stateController.stream;
  Sink<EventsBloc> get action => _eventController.sink;

  CounterBlocState() {
    _eventController.stream.listen(_handleEvent);
  }

  void dispose() {
    _stateController.close();
    _eventController.close();
  }

  void _handleEvent(EventsBloc action) {
    if (action == EventsBloc.increase) {
      value++;
    }
    _stateController.add(value);
  }
}
